<?php

function getFullAddress($specific,$city,$province,$country){
    return "$specific, $city, $province, $country";
}

function getLetterGrade($score){
    if($score <= 74 && $score > 0){
        return "F";
    }
    if($score >= 75 && $score <= 76){
        return "C-";
    }
    if($score >76 &&  $score <= 79){
        return "C";
    }
    if($score >79 &&  $score <= 82){
        return "C+";
    }
    if($score >82 &&  $score <= 85){
        return "B-";
    }
    if($score >85 &&  $score <= 88){
        return "B";
    }
    if($score >88 &&  $score <= 91){
        return "B+";
    }
    if($score >91 &&  $score <= 94){
        return "A-";
    }
    if($score >94 &&  $score <= 97){
        return "A";
    }
    if($score >97 &&  $score <= 100){
        return "A+";
    }
    if($score > 100 || $score < 0 ){
        return "Invalid Score";
    }
}


?>