
<!-- PHP code can be included to another file by using the require_once keyword. -->
<!-- We will use this method to separate the declaration of variables and functions from the HTML content. -->
<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
    <h1>Full Address</h1>
    <p> Address: <?=  getFullAddress('3F Casawynn Bldg,Timog Avenue','Quezon City','Metro Manila','Philippines')?></p>
    <h1> Letter Grading </h1>
    <p><?= getLetterGrade(92)?></p>

</body>
</html>